const { fibonacci } = require("..");

it('should throw error from 0 parameter', () => {
    expect(() => fibonacci(0)).toThrow('[n] must be bigger then zero');
});

it('should throw error from -42 parameter', () => {
    expect(() => fibonacci(-42)).toThrow('[n] can\'t be negative');
});

it('should throw error from null parameter', () => {
    expect(() => fibonacci(null)).toThrow('[n] can\'t be null or undefined');
});

it('should throw error from undefined parameter', () => {
    expect(() => fibonacci()).toThrow('[n] can\'t be null or undefined');
});

it('should throw error from not number parameter', () => {
    expect(() => fibonacci('a')).toThrow('[n] should be a number');
});

it('should throw error from decimal number', () => {
    expect(() => fibonacci(1.2)).toThrow('[n] should be an integer');
});

it('should return first element from fibonacci', () => {
    expect(fibonacci(1)).toBe(0);
});

it('should return second element from fibonacci', () => {
    expect(fibonacci(2)).toBe(1);
});

it('should return third element from fibonacci', () => {
    expect(fibonacci(3)).toBe(1);
});

it('should return forth element from fibonacci', () => {
    expect(fibonacci(4)).toBe(2);
});

it('should return fifth element from fibonacci', () => {
    expect(fibonacci(5)).toBe(3);
});

it('should return ninth eight from fibonacci', () => {
    expect(fibonacci(8)).toBe(13);
});

it('should return twelve element from fibonacci', () => {
    expect(fibonacci(12)).toBe(89);
});

it('should return eighteenth element from fibonacci', () => {
    expect(fibonacci(18)).toBe(1597);
});

